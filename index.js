/*
MIT License

Copyright (c) 2017 Spencer Sager

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

// Load discord.js and create client
const Discord = require("discord.js");
const client = new Discord.Client();

// Load chalk for console output stylings
const chalk = require("chalk");

// Load moment and node-schedule for timed output
const Moment = require("moment");
const Schedule = require("node-schedule");

// Load config file containing token and prefix
const config = require("./config.json");

// Triggers when the bot starts and logs in successfully
client.on("ready", () => {
  // Start-up information, including versioning
  console.log(
    chalk.bold.white(
      `Starting ${chalk.red(config.username)}...\n\t` +
        `Node version: ${chalk.hex("#43853D").underline(process.version)}\n\t` +
        `Discord version: ${chalk
          .hex("#7289DA")
          .underline("v" + Discord.version)}\n`
    )
  );
  // Attempt to set username to name specified in config.json
  client.user.setUsername(config.username).catch(error => {
    console.log(
      chalk.bold.white(`Error changing username: ${chalk.red(error.message)}`)
    );
  });
  // Attempt to set game to bot flavor text
  client.user.setGame(config.flavor_text).catch(error => {
    console.log(
      chalk.bold.white(`Error changing game: ${chalk.red(error.message)}`)
    );
  });
  // Attempt to set bot status to online
  client.user.setStatus("online").catch(error => {
    console.log(
      chalk.bold.white(`Error changing status: ${chalk.red(error.message)}`)
    );
  });
  // Give final ready message once the above have been handled
  console.log(
    chalk.bold.white(
      `${chalk.red(config.username)} is now serving ${chalk.red(
        client.guilds.size
      )} server(s)! Prefix: ${chalk.red(config.prefix)}\n`
    )
  );
});

// Triggers when the bot joins a guild
client.on("guildCreate", guild => {
  console.log(
    chalk.bold.white(
      `New guild joined: ${chalk.red(guild.name)} (id: ${chalk.red(
        guild.id
      )} | members: ${chalk.red(guild.memberCount - 1)})`
    )
  );
});

// Triggers when the bot leaves a guild
client.on("guildDelete", guild => {
  console.log(
    chalk.bold.white(
      `I have been removed from ${chalk.red(guild.name)} (id: ${chalk.red(
        guild.id
      )} | members: ${chalk.red(guild.memberCount)})`
    )
  );
});

// Triggers on message received, from channels or DM
client.on("message", async message => {
  // Ignore bot commands, including our own
  if (message.author.bot) return;

  // Ignore messages not starting with a prefix
  if (message.content.indexOf(config.prefix) !== 0) return;

  // Separate command name from arguments
  const args = message.content
    .slice(config.prefix.length)
    .trim()
    .split(/ +/g);
  const command = args.shift().toLowerCase();

  // Ping command
  if (command === "ping") {
    // Delete the message that invoked command
    message.delete();
    // Send an initial message
    const m = await message.channel.send("Ping?");
    // Edit the message and inject a rich embed object
    m.edit({
      embed: {
        color: 4437377,
        author: {
          name: client.user.username,
          icon_url: client.user.avatarURL
        },
        title: "Pong!",
        description:
          "I've crunched the numbers for you, and here's what I found:",
        fields: [
          {
            name: "Discord Latency", // Difference between sent timestamp and edited timestamp
            value: `Round-trip edit time: **${Math.round(
              (m.createdTimestamp - message.createdTimestamp) / 2
            )}ms**`
          },
          {
            name: "API Latency", // API function finding bot delay
            value: `Discord.js delay: **${Math.round(client.ping)}ms**`
          }
        ],
        timestamp: new Date(),
        footer: {
          text: client.user.username
        }
      }
    });
  }

  // Say command
  if (command === "say") {
    // Join the arguments with spaces to form a coherent string
    const sayMessage = args.join(" ");
    // Delete the message that invoked the command
    message.delete();
    // Send the message
    message.channel.send(sayMessage);
  }
});

client.login(config.token);
